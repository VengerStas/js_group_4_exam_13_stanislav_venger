import axios from '../../axiosBase';
import {NotificationManager} from "react-notifications";

export const FETCH_GALLERY_SUCCESS = 'FETCH_GALLERY_SUCCESS';


export const fetchGallerySuccess = gallery => ({type: FETCH_GALLERY_SUCCESS, gallery});

export const getGallery = (placeId) => {
    return dispatch => {
        return axios.get('/gallery/' + placeId).then(
            response => {
                dispatch(fetchGallerySuccess(response.data))
            }
        );
    };
};

export const addImage = (imageData, placeId) => {
    return dispatch => {
        return axios.post('/gallery', imageData).then(() => {
            dispatch(getGallery(placeId));
        })
    }
};

export const deleteGallery = (galleryId, placeId) => {
    return (dispatch) => {
        return axios.delete('/gallery/' + galleryId).then(() => {
            NotificationManager.success('Image has been deleted');
            dispatch(getGallery(placeId));
        })
    }
};