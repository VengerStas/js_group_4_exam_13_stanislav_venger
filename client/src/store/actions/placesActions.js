import {NotificationManager}  from 'react-notifications';
import axios from '../../axiosBase';
import {push} from "connected-react-router";

export const FETCH_PLACES_SUCCESS = 'FETCH_PLACES_SUCCESS';
export const FETCH_PLACE_SUCCESS = 'FETCH_PLACE_SUCCESS';
export const FETCH_PLACE_FAILURE = 'FETCH_PLACE_FAILURE';


export const fetchPlacesSuccess = places => ({type: FETCH_PLACES_SUCCESS, places});
export const fetchPlaceSuccess = place => ({type: FETCH_PLACE_SUCCESS, place});
export const fetchPlaceFailure = error => ({type: FETCH_PLACE_FAILURE, error});

export const getPlaces = () => {
    return dispatch => {
        return axios.get('/places').then(
            response => {
                dispatch(fetchPlacesSuccess(response.data))
            }
        );
    };
};

export const getPlace = (placeId) => {
    return dispatch => {
        return axios.get('/places/' + placeId).then(
            response => {
                dispatch(fetchPlaceSuccess(response.data))
            }
        )
    }
};

export const placeCreate = (placeData) => {
    return dispatch => {
        return axios.post('/places', placeData).then(
            (response) => {
                dispatch(getPlaces(response.data.user._id));
                NotificationManager.success('New place created!');
                dispatch(push('/'));
            },
            error => {
                if (error.response) {
                    dispatch(fetchPlaceFailure(error.response.data));
                    NotificationManager.error('Need to fill in all the fields!');
                } else {
                    dispatch(fetchPlaceFailure({Global: 'No connection'}))
                }
            }
        )
    }
};

export const deletePlace = (placeId) => {
    return (dispatch) => {
        return axios.delete('/places/' + placeId).then(() => {
            NotificationManager.success('Place has been deleted');
            dispatch(getPlaces());
            dispatch(push('/'));
        })
    }
};