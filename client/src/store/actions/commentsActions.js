import axios from '../../axiosBase';
import {NotificationManager} from "react-notifications";

export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchCommentsSuccess = comments => ({type: FETCH_COMMENTS_SUCCESS, comments});

export const getComments = (placeId) => {
    return dispatch => {
        return axios.get('/comments/' + placeId).then(
            response => {
                dispatch(fetchCommentsSuccess(response.data))
            }
        );
    };
};

export const commentsCreate = (commentsData) => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        const placeId = commentsData.place;
        return axios.post('/comments', commentsData, {headers: {'Authorization': token}}).then(() => {
            dispatch(getComments(placeId));
        })
    }
};

export const deleteComments = (commentId, placeId) => {
    return (dispatch) => {
        return axios.delete('/comments/' + commentId).then(() => {
            NotificationManager.success('Comment has been deleted');
            dispatch(getComments(placeId));
        })
    }
};