#!/bin/bash

cd ../api

NODE_ENV=test yarn run seed

cd ../tests

npx codeceptjs run -f $1 --steps
