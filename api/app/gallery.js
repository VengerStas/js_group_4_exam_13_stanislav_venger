const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Gallery = require('../models/Gallery');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadGalleryPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/:id', async (req, res) => {
    Gallery.find({place: req.params.id}).populate('user', '_id, displayName').populate('place', '_id')
        .then(result => res.send(result))
        .catch(error => {
            res.status(400).send(error)
        } )
});

router.post('/', auth, upload.array('gallery', 10), (req, res) => {
    const galleryData = {
        gallery: [],
        user: req.user._id,
        place: req.body.place
    };

    if (req.files) {
        galleryData.gallery = req.files.map(file => file.filename)
    }

    const gallery = new Gallery (galleryData);

    gallery.save()
        .then(results => res.send(results))
        .catch(error => {
            res.status(400).send(error)
        } );
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Gallery.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});


module.exports = router;