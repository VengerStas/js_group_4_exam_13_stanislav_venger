const express = require('express');
const auth = require('../middleware/auth');
const Comments = require('../models/Comments');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/:id', (req, res) => {
    Comments.find({place: req.params.id}).populate('user', '_id, displayName').populate('place', '_id, title').sort({datetime: -1})
        .then(result => res.send(result))
        .catch(error => res.status(500).send(error))
});

router.post('/', auth, (req, res) => {
    const commentData = {...req.body, user: req.user._id, datetime: new Date().toISOString()};

    const comments = new Comments (commentData);

    comments.save()
        .then(results => res.send(results))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Comments.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;