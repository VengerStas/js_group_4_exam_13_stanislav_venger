const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Place = require('./models/Place');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOption);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users =  await User.create ({
        username: 'user1',
        password: '123',
        displayName: 'User 1',
        role: 'admin',
        token: nanoid()
    }, {
        username: 'user2',
        password: '123',
        displayName: 'User 2',
        role: 'user',
        token: nanoid()
    });

    await Place.create(
        {user: users[0]._id, title: 'Кальянный бар "Bunker"', desc: 'Профессиональные кальяны, Богатый выбор чаев, Фирменный BunkerBurg, Официальные представители табака Satyr в КР.', image: 'bunker.jpg'},
        {user: users[1]._id, title: 'Бублик', desc: '«Бублик» — небольшое уютное заведение, открывшееся в центре Бишкека. Новое место, в первую очередь, отличает подход к кофейной обжарке, здесь это делается на основе зерен Starbucks. Я - кофейня в самом центре города. Я - аромат настоящего кофе. Я - это уютная домашняя атмосфера. Я - Бублик!', image: 'bublik.jpg'}
    );

    return connection.close();
};

run().catch(error => {
    console.log('Oops something happened...', error);
});