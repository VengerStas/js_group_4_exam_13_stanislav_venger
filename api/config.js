const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    uploadGalleryPath: path.join(rootPath, 'public/uploads/gallery/'),
    dbUrl: 'mongodb://localhost/critic',
    mongoOption: { useNewUrlParser: true, useCreateIndex: true},
};